const fs = require('fs');
const discord = require('discord.js');
const client = new discord.Client();

function loadConfig(path) {
    let data = fs.readFileSync(path);
    let json = JSON.parse(data);
    if (!json.token) {
        throw "Missing token";
    }

    if (!json.targetId) {
        throw "Missing targetId";
    }

    return json;
}

let config;

try {
    config = loadConfig('config.json');
} catch (e) {
    console.error(`Unable to load config, got error: ${e}`);
    return;
}

client.on('ready', () => console.log(`Logged in as ${client.user.tag}!`));
client.on('message', msgHandler);

console.log("Logging in...");
client
    .login(config.token)
    .catch((e) => console.error(`Failed to send login details, got error: ${e}`));

function msgHandler(msg) {
    if (msg.channel.type != 'group') {
        return;
    }

    if (msg.type != "RECIPIENT_ADD") {
        return;
    }

    if (!msg.isMentioned(config.targetId)) {
        return;
    }

    let user = client.users.get(config.targetId);
    let channel = msg.channel;

    channel.removeUser(user).then(function(c) {
        console.log(`Successfully removed ${user.tag}`)
    }).catch(function(e) {
        console.log(`Failed to remove ${user.tag}, got error: `, e);
    });
}